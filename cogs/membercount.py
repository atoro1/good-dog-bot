import discord
from discord.ext import commands

membercount_help = "Shows the total number of members in the server"

class MemberCount:
    def __init__(self, client):
        self.client = client

    @commands.command(help=membercount_help)
    async def membercount(self, ctx, *args):
	    await ctx.send("We have {} members".format(ctx.message.guild.member_count)

def setup(client):
    client.add_cog(MemberCount(client))