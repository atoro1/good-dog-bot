import discord
from discord.ext import commands

ping_help = "You ping, it pongs! Shows latency of the bot"

class PingCog:
    def __init__(self, client):
        self.client = client

    @commands.command(help=ping_help)
    async def ping(self, ctx, *args):
	    await ctx.send(":ping_pong: Pong! `{} ms`".format(self.client.latency * 1000))
	    
def setup(client):
    client.add_cog(PingCog(client))