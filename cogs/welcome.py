import discord, yaml
from datetime import datetime
from discord.ext import commands

with open("settings.yml", 'r') as config:
    settings = yaml.load(config)

class Welcome:
    def __init__(self, client):
        self.client  = client
        self.welcome = settings['welcome_channel']
        self.errors  = settings['error_lot_channel']

    async def on_member_join(self, member):
        try:
            await self.client.get_channel(self.welcome).send(settings['welcome_message'])
        except Exception as e:
            await self.client.get_channel(self.errors).send("Error in {}: {}".format(self.client.get_channel(self.welcome).mention, str(e)))

def setup(client):
    client.add_cog(Welcome(client))